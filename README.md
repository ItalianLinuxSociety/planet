# Planet LUG

Planet LUG è un aggregatore di feeds RSS (eseguito tutti i giorni a mezzanotte), che colleziona le news
provenienti dai siti dei LUG e dei soci dell'associazione Italian Linux
Society.

L'elenco dei feeds inclusi viene periodicamente e automaticamente
estrapolato dalla LugMap e dal gestionale dei soci ILS.

Visita il sito [https://planet.linux.it](https://planet.linux.it/)

## Installazione

* Eseguire `composer install`
* Copiare il file `config.sample.php` in `config.php`
* Richiede un file `opml.xml` nelle cartelle `ils` e `lug`
  * Scaricando i file dalla produzione il token di ILS Manager non è necessario
    * https://planet.linux.it/ils/opml.xml (Dalla root del progetto puoi usare il comando `wget -P ils/ https://planet.linux.it/ils/opml.xml`)
    * https://planet.linux.it/lug/opml.xml (Dalla root del progetto puoi usare il comando `wget -P lug/ https://planet.linux.it/lug/opml.xml`)
* Eseguire `generate_contents.php` per generare i file

## Comunicazione con ILS Manager

Il Planet comunica con [ILS Manager](https://gitlab.com/ItalianLinuxSociety/ilsmanager) utilizzando questa chiamata API:

    https://ilsmanager.linux.it/ng/api/websites?token=<token>

Per funzionare, nel file `config.php` ci deve essere la costante `ILSMANAGER_TOKEN`. Questo token deve essere preso dalla configurazione di ILS Manager chiamata `API_TOKEN`.

### Ordinare il file calendars.txt

Per non dover ordinare lato codice il file è necessario eseguire questo comando:

```shell
$ cat eventi/calendars.txt | (read -r; printf "%s\n" "$REPLY"; sort)
```
