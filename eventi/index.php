<?php

require_once ('../funzioni.php');
lugheader ('Calendario', 'Eventi del mondo FOSS italiano');

?>

<div class="container main-contents">
    <div class="row">
        <div class="col-12">Feed Nazionale <a href="webcal://planet.linux.it/eventi/eventi.ics"><img src="/images/calendar-icon.png" alt="Planet" style="margin-top: -8px;margin-right: 4px;width: 16px;"></a><a href="https://planet.linux.it/eventi/eventi.rss"><img src="/images/feed-icon.png" alt="Planet" style="margin-top: -8px;width: 16px;"></a> - Contribuisci al planet su <a rel="nofollow" href="https://gitlab.com/ItalianLinuxSociety/Planet"><img src="https://www.linux.it/shared/index.php?f=immagini/gitlab.svg" style="height:20px;margin-top: -8px;" alt="Planet su GitLab"></a>
        <a href="#" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#istruzioni">Come funziona? <span class="arrow-animation">←</a></a>
        <a href="#" class="btn btn-sm btn-default float-right" data-toggle="modal" data-target="#feed">Feed Regionali</a></div>
        </div>
    <div class="row">
        <div class="col-12" style="overflow-x: hidden">
            <div id="calendar"></div>
        </div>
    </div>
	<?php 
	if ( file_exists( 'events.html' ) ) {
        if (!isset($_GET['regione'])) {
            include('events.html');
        }
    } else {
        echo 'Generazione degli eventi mancante';
    }
    if (isset($_GET['regione'])) {
        echo '<div class="col-12">';
    }
    include('modali.html');
	?>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

