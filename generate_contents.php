<?php

if (!in_array(PHP_SAPI, ['cli', 'phpdbg', 'embed'], true)) {
    echo 'Error: The console should be invoked via the CLI version of PHP, not the '.PHP_SAPI.' SAPI'.PHP_EOL;
    die();
}

require_once('funzioni.php');

$path = 'lug/opml.xml';
if ( file_exists( $path ) ) {
    echo "Elaborazione LUG OPML\n";
    $data = retrieveContents($path, 30);
    if ( $data != false) {
        formatPage($path, $data, 'lug/contents.html');
        formatFeed($data, 'Planet LUG', 'lug/rss.xml');
        echo "Lug RSS finito\n";
    } else {
        echo "ILS OPML danneggiato o non valido\n";
    }
} else {
    echo "Elaborazione LUG OPML non effettuata\n";
}


$path = 'ils/opml.xml';
if ( file_exists( $path ) ) {
    echo "Elaborazione ILS OPML\n";
    $data = retrieveContents($path, 30);
    if ( $data != false) {
        formatPage($path, $data, 'ils/contents.html');
        formatFeed($data, 'Planet ILS', 'ils/rss.xml');
        echo "ILS RSS finito\n";
    } else {
        echo "ILS OPML danneggiato o non valido\n";
    }
} else {
    echo "Elaborazione ILS OPML non effettuata\n";
}

echo "Elaborazione Calendario\n";
$path = 'eventi/calendars.txt';
$data = retrieveCalContents($path, 30);
formatCalPage($path, $data, 'eventi/events.html');

echo "Calendari finito\n";
