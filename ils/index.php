<?php

require_once ('../funzioni.php');
lugheader ('Planet ILS', 'News dai soci di Italian Linux Society');

?>

<div class="container main-contents">
	<?php 
	if ( file_exists( 'contents.html' ) ) {
        include('contents.html');
    } else {
        echo 'Generazione degli eventi mancante';
    }
    ?>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

